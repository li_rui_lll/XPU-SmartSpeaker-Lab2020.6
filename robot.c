#include <stdio.h>  //fgets
#include <string.h> //strlen
#include "cJSON.h"

/*
 构造JSON请求报文
 {
    "perception": {
        "inputText": {
            "text": "你好"
        }
    },
    "userInfo": {
        "apiKey": "xxx",
        "userId": "liuyu"
    }
 }
 */
char* robot_make_request(const char* apikey, const char* text)
{
    //判断输入的字符串长度不为0
    if (strlen(text) == 0)
    {
        return NULL;
    }

    cJSON* request = cJSON_CreateObject();

    cJSON* perception = cJSON_CreateObject();
    cJSON* inputText = cJSON_CreateObject();

    cJSON_AddStringToObject(inputText, "text", text);
    cJSON_AddItemToObject(perception, "inputText", inputText);
    cJSON_AddItemToObject(request, "perception", perception);

    cJSON* userInfo = cJSON_CreateObject();
    cJSON_AddStringToObject(userInfo, "apiKey", apikey);
    cJSON_AddStringToObject(userInfo, "userId", "liuyu");
    cJSON_AddItemToObject(request, "userInfo", userInfo);

    //将JSON数据结构转为字符串
    return cJSON_Print(request);
}

//作业：发送请求报文给图灵机器人服务器，等待服务器的响应报文
//     收到响应报文后，不需要解析，直接通过函数返回返回JSON字符串
char* robot_send_request(const char* request)
{
}

//图灵机器人API，一次最多可以处理128个字符
#define LINE_LEN 128

//保存输入字符串的缓冲区
char line[LINE_LEN];

int main()
{
    char* apikey = "自己创建机器人的APIKEY";
    //从标准输入读取一行字符
    while(fgets(line, LINE_LEN, stdin) != NULL)
    {
        //构造请求报文
        char* request = robot_make_request(apikey, line);
        if (request == NULL)
        {
            continue;
        }
        //将请求报文发送给图灵机器人
        robot_send_request(request);
    }

    return 0;
}